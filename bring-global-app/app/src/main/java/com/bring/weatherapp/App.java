package com.bring.weatherapp;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;

import io.fabric.sdk.android.Fabric;

public class App extends Application {



    // Debugging tag for the application
    public static final String TAG = "BRING_DEMO";

    private static final String PREFS_NAME = "PrefsUserFile";
    private static SharedPreferences mPref;

    private static App sInstance;
    private static String appVersion;

    public static FirebaseAnalytics mFirebaseAnalytics;

    public static String citySelected;


    public static String[] arrayUnitsSystem = {"Fahrenheit", "Celsius"};

    @Override
    public void onCreate() {
        super.onCreate();


        sInstance = this;

        // Crashlytics
        Fabric.with(this, new Crashlytics());

        // FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Restore preferences
        mPref = getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);


    }

    public static App getInstance() {
        return sInstance;
    }

    public static String getAppVersion() {

        appVersion = "";

        try {
            appVersion = sInstance.getPackageManager().getPackageInfo(sInstance.getPackageName(),
                    0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

            //TODO
            //Crashlytics.logException(e);
            e.printStackTrace();
        }

        return appVersion;
    }


    /*
     *
     *  For the purpose of this app demo
     *  production and development are the same
     */
    public static boolean isAPILive() {

        if (getInstance().getResources().getBoolean(R.bool.flag_api_live)) {
            return true;
        } else {
            return false;
        }
    }



    /*
     * Get API url (LIVE or TEST)
     *  For the purpose of this app demo
     *  production and development are the same
     */
    public static String getAPIurl() {

        boolean liveMode = isAPILive();

        if (liveMode) {
            return getInstance().getString(R.string.api_url_production);
        } else {
            return getInstance().getString(R.string.api_url_test);
        }
    }


    /*
     *  API key provided by Global Bring
     *  for the purpose of this Demo App
     */
    public static String getAPIkey() {

        return getInstance().getString(R.string.api_key);
    }




    public static SharedPreferences getSharedPreferences() {

        if (mPref == null) {
            // Restore preferences
            mPref = getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        }

        return mPref;
    }


}