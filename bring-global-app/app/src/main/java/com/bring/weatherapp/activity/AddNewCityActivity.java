package com.bring.weatherapp.activity;


import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bring.weatherapp.App;
import com.bring.weatherapp.R;
import com.bring.weatherapp.adapter.CityAdapter;
import com.bring.weatherapp.api.FindWeatherAPI;
import com.bring.weatherapp.fragment.CityFragment;
import com.bring.weatherapp.fragment.HomeFragment;
import com.bring.weatherapp.fragment.SettingsFragment;
import com.bring.weatherapp.fragment.UserPrefFragment;
import com.bring.weatherapp.model.APIError;
import com.bring.weatherapp.model.FindCitiesObject;
import com.bring.weatherapp.model.WeatherObject;
import com.bring.weatherapp.util.Utils;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

public class AddNewCityActivity extends AppCompatActivity {


    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private CityAdapter adapter;
    private LinearLayout mEmptyListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new);


        // Bind views
        mRecyclerView = findViewById(R.id.recyclerView);
        mProgressBar = findViewById(R.id.progressBar);
        mEmptyListView = findViewById(R.id.ll_empty_list);


        mEmptyListView.setVisibility(View.GONE);


    }


    @Override
    public void onResume() {
        super.onResume();

        getData();
    }


    /*
     * Update UI
     */
    private void updateUI(FindCitiesObject w) {


        List<WeatherObject> list = w.getList();
        if (list != null && list.size() > 0) {

            mRecyclerView.setVisibility(View.VISIBLE);

            // set up the RecyclerView
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
            adapter = new CityAdapter(this, getSupportFragmentManager(), list);
            mRecyclerView.setAdapter(adapter);

        } else {
            displayEmptyList();
        }


    }

    private void displayEmptyList() {

        mRecyclerView.setVisibility(View.GONE);
        mEmptyListView.setVisibility(View.VISIBLE);

    }

    /*
     * Display ProgressBar on screen
     * while the Data is fetching
     */
    private void loadingProgressBar(boolean isLoading) {

        if (isLoading) {

            mProgressBar.setVisibility(View.VISIBLE);
            mEmptyListView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);


            if (adapter != null) {
                adapter.clearAll();
            }

        } else {

            mProgressBar.setVisibility(View.GONE);

        }

    }

    /*
     * GET Data
     *
     */
    private void getData() {


        // Demo purpose to the location of Lisbon/Portugal
        String lat = "38.71";
        String lon = "-9.14";

        int count = 50; // Demo purpose to display 50 rows of data


        // Display Loading...
        loadingProgressBar(true);

        FindWeatherAPI.getCities(lat, lon, count, new FindWeatherAPI.VolleyCallback() {
            @Override
            public void onSuccess(FindCitiesObject object) {

                // SUCCESS :)
                // Loading...
                loadingProgressBar(false);

                updateUI(object);
            }

            @Override
            public void onFailure() {

                // Loading...
                loadingProgressBar(false);
                String msg = getString(R.string.failure_toast_message);
                Toast.makeText(App.getInstance(), msg, Toast.LENGTH_LONG).show();

                Crashlytics.log(msg);
            }

            @Override
            public void onError(APIError error) {
                // Loading...
                loadingProgressBar(false);
                String msg = getString(R.string.error_toast_message);

                if (error != null) {
                    msg = Utils.toString(error.getError().getMessage());
                }

                Toast.makeText(App.getInstance(), msg, Toast.LENGTH_LONG).show();

                Crashlytics.log(msg);
            }
        });



    }



}
