package com.bring.weatherapp.activity;


import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bring.weatherapp.App;
import com.bring.weatherapp.R;
import com.bring.weatherapp.fragment.CityFragment;
import com.bring.weatherapp.fragment.HomeFragment;
import com.bring.weatherapp.fragment.SettingsFragment;
import com.bring.weatherapp.fragment.UserPrefFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Bind views
        bottomNavigationView = findViewById(R.id.navigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        updateUI();


    }

    private void updateUI() {


        // Displays Always
        bottomNavigationView.setVisibility(View.VISIBLE);

        // programmatically click on the BottomNavigationBar :)
        View view = bottomNavigationView.findViewById(R.id.navigation_home);
        view.performClick();

    }


    private Boolean exit = false;

    @Override
    public void onBackPressed() {


        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(App.getInstance(), "Press Back again to Exit the app.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }


    }

    // Displays User Preference Fragment
    //
    public void displayUserPrefFragment() {

        Fragment fragment = UserPrefFragment.newInstance();
        loadFragment(fragment);
    }

    // programmatically click on the City Tab
    // on the BottomNavigationBar :)
    public void programmaticallyClickCityTab() {


        View view = bottomNavigationView.findViewById(R.id.navigation_city);
        view.performClick();
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;

                switch (item.getItemId()) {
                    case R.id.navigation_home:


                        fragment = HomeFragment.newInstance();
                        loadFragment(fragment);
                        return true;


                    case R.id.navigation_city:

                        if(!item.isChecked()) {
                            fragment = CityFragment.newInstance();
                            loadFragment(fragment);
                            return true;
                        } else {
                            return false;
                        }

                    case R.id.navigation_settings:

                        fragment = SettingsFragment.newInstance();
                        loadFragment(fragment);
                        return true;

            }


            return false;
        }
    };


    public void loadFragment(Fragment fragment) {

        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
