package com.bring.weatherapp.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bring.weatherapp.App;
import com.bring.weatherapp.R;
import com.bring.weatherapp.activity.AddNewCityActivity;
import com.bring.weatherapp.activity.MainActivity;
import com.bring.weatherapp.model.WeatherObject;
import com.bring.weatherapp.util.SaveLocal;
import com.bring.weatherapp.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private List<WeatherObject> mData;
    private LayoutInflater mInflater;
    private Activity mActivity;
    private FragmentManager mFragmentManager;
    private Boolean isEditable;

    // data is passed into the constructor
    public CityAdapter(Activity activity, FragmentManager fragmentManager, List<WeatherObject> data) {
        this.mActivity = activity;
        this.mFragmentManager = fragmentManager;
        this.mInflater = LayoutInflater.from(activity);
        this.mData = data;
        this.isEditable = false; //by default
    }


    public void setEditable(Boolean isIt) {

        isEditable = isIt;
    }

    public Boolean isEditable() {

        return isEditable;
    }

    public void setData(List<WeatherObject> list) {

        mData = list;

    }

    public void clearAll() {

        if (mData != null) {
            mData.clear();
        }
    }


    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_city_weather, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        WeatherObject w = mData.get(position);

        holder.obj = w;


        // set views
        holder.tvCityName.setText(w.getName());
        holder.tvTemp.setText(w.getMain().getTemp() + "°");

        if (w.getWeather().size() > 0) {
            holder.tvStatus.setText(w.getWeather().get(0).getMain());

            // Image URL
            String imgURL = Utils.getIconURL(w.getWeather().get(0).getIcon());
            // Display Image
            if (!Utils.toString(imgURL).isEmpty()) {
                Picasso.with(mActivity).load(imgURL)
                        .into(holder.ivIcon);
            }

        }

        if (isEditable) {
            holder.ivRemove.setVisibility(View.VISIBLE);
            holder.ivRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Delete from the list
                    SaveLocal.removeCitySearched(w); // Remove into SharedPreference (as persistent data)
                    setData(SaveLocal.getListCitiesSearched());
                    notifyDataSetChanged();

                }
            });
        } else {
            holder.ivRemove.setVisibility(View.GONE);
            holder.ivRemove.setOnClickListener(null);
        }


    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        WeatherObject obj;
        LinearLayout llCardView;
        TextView tvCityName;
        TextView tvTemp;
        TextView tvStatus;
        ImageView ivIcon;
        ImageView ivRemove;

        ViewHolder(View itemView) {
            super(itemView);

            llCardView = itemView.findViewById(R.id.ll_card_view);
            tvCityName = itemView.findViewById(R.id.tv_city_name);
            tvTemp = itemView.findViewById(R.id.tv_temperature);
            tvStatus = itemView.findViewById(R.id.tv_weather_status);
            ivIcon = itemView.findViewById(R.id.iv_weather_icon);
            ivRemove = itemView.findViewById(R.id.iv_remove);

            llCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            String id = String.valueOf(obj.getId());
            String cityName = obj.getName();
            Log.d(App.TAG, "Clicked  Id = " + id);
            Log.d(App.TAG, "Clicked  Name = " + cityName);


            if (mActivity instanceof AddNewCityActivity) {

                SaveLocal.saveInCitiesSearched(obj); // Save into SharedPreference (as persistent data)
                mActivity.finish(); // just finish ;) The same as going back previous activity

            } else {

                App.citySelected = cityName;
                ((MainActivity) mActivity).programmaticallyClickCityTab();
            }


        }
    }


}
