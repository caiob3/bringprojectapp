package com.bring.weatherapp.api;


import android.util.Log;

import com.bring.weatherapp.App;
import com.bring.weatherapp.model.APIError;
import com.bring.weatherapp.model.WeatherObject;
import com.bring.weatherapp.network.Service;
import com.bring.weatherapp.util.ErrorUtils;
import com.bring.weatherapp.util.SaveLocal;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by caio
 */
public class WeatherAPI {


    public interface VolleyCallback {

        void onSuccess(WeatherObject weatherObject);

        void onFailure();

        void onError(APIError error);
    }



    /* GET Weather City Detail */
    public static void geWeatherCityDetail(final String cityName, final VolleyCallback callback) {


        String units = SaveLocal.getUnitSystemPreference();
        // API Endpoint
        Call<JsonObject> call = Service.getService().getByCityName(cityName, units);


        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


                try {

                    if (response.isSuccessful()) {


                        // :)
                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                        WeatherObject w = gson.fromJson(response.body(), WeatherObject.class);


                        callback.onSuccess(w);


                    } else {

                        // parse the response body …
                        APIError error = ErrorUtils.parseError(response);
                        // … object to use it to show error information

                        callback.onError(error); // in case error is NULL, it's handle on the callback of this ;)
                    }


                } catch (Exception e) {

                    Log.e(App.TAG, "Exception: " + e.getMessage());
                    Crashlytics.logException(e);

                    callback.onFailure();

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                Log.e(App.TAG, "onFailure: " + t.getMessage());
                Crashlytics.logException(t);
                callback.onFailure();

            }
        });

    }

}
