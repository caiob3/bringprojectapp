package com.bring.weatherapp.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bring.weatherapp.App;
import com.bring.weatherapp.R;
import com.bring.weatherapp.api.WeatherAPI;
import com.bring.weatherapp.model.APIError;
import com.bring.weatherapp.model.WeatherObject;
import com.bring.weatherapp.util.SaveLocal;
import com.bring.weatherapp.util.Utils;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

/**
 * Fragment
 *
 * @author Caio Bulgarelli
 */
public class CityFragment extends Fragment {


    private ProgressBar mProgressBar;
    private LinearLayout mBody;

    private TextView cityText;
    private TextView feelsLike;
    private TextView temp;
    private TextView hum;
    private TextView press;
    private TextView windSpeed;
    private TextView weatherDesc;
    private ImageView ivIconWeather;


    public static final CityFragment newInstance() {

        CityFragment thisFragment = new CityFragment();

        Bundle args = new Bundle();
        thisFragment.setArguments(args);

        return thisFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_city, null);

        //Bind Views
        mProgressBar =  v.findViewById(R.id.progressBar);
        mBody =  v.findViewById(R.id.ll_body);
        feelsLike =  v.findViewById(R.id.feels_like);
        cityText =  v.findViewById(R.id.cityText);
        temp =  v.findViewById(R.id.temp);
        hum =  v.findViewById(R.id.hum);
        press =  v.findViewById(R.id.press);
        windSpeed =  v.findViewById(R.id.windSpeed);
        weatherDesc =  v.findViewById(R.id.tv_weather_desc);
        ivIconWeather =  v.findViewById(R.id.iv_condIcon);



        return v;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        String city = Utils.toString(App.citySelected);

        getData(city);
    }


    /*
     * Update UI
     */
    private void updateUI(WeatherObject w) {

        String speedLabel = "";
        if (SaveLocal.getUnitSystemPreference().equalsIgnoreCase("imperial")) {
            speedLabel = "miles/hour";
        } else {
            speedLabel = "meter/sec";
        }


         cityText.setText(w.getName());
         feelsLike.setText(w.getMain().getFeelsLike() + "°");
         temp.setText(w.getMain().getTemp() + "°");
         hum.setText( w.getMain().getHumidity() + "") ;
         press.setText(w.getMain().getPressure() + "");
         windSpeed.setText(w.getWind().getSpeed() + speedLabel + ", " + w.getWind().getDeg() + "°");

         if (w.getWeather().size() >0) {
             weatherDesc.setText(w.getWeather().get(0).getDescription());
         }



        // Image URL
        if (w.getWeather().size() > 0) {
            String imgURL = Utils.getIconURL(w.getWeather().get(0).getIcon());
            // Display Image
            if (!Utils.toString(imgURL).isEmpty()) {
                Picasso.with(getActivity()).load(imgURL)
                        .into(ivIconWeather);
            }
        }

    }

    /*
     * Display ProgressBar on screen
     * while the Data is fetching
     */
    private void loadingProgressBar(boolean isLoading) {

        if (isLoading) {

            mProgressBar.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.GONE);

        } else {

            mProgressBar.setVisibility(View.GONE);
            mBody.setVisibility(View.VISIBLE);

        }

    }

    /*
     * GET Data
     *
     */
    private void getData(String city) {


        if (city.isEmpty()) {

            if (SaveLocal.getListCitiesSearched().size() > 0)
            {
                city = SaveLocal.getListCitiesSearched().get(0).getName(); // default is the first of the list
            } else {
                city = "Lisbon"; //by default first time
            }
        }

        // Display Loading...
        loadingProgressBar(true);

        WeatherAPI.geWeatherCityDetail(city, new WeatherAPI.VolleyCallback() {
            @Override
            public void onSuccess(WeatherObject weatherObject) {


                // SUCCESS :)
                // Loading...
                loadingProgressBar(false);

                updateUI(weatherObject);
            }

            @Override
            public void onFailure() {

                // Loading...
                loadingProgressBar(false);
                String msg = getActivity().getString(R.string.failure_toast_message);
                Toast.makeText(App.getInstance(), msg, Toast.LENGTH_LONG).show();

                Crashlytics.log(msg);
            }

            @Override
            public void onError(APIError error) {

                // Loading...
                loadingProgressBar(false);
                String msg = getActivity().getString(R.string.error_toast_message);

                if (error != null) {
                    msg = Utils.toString(error.getError().getMessage());
                }

                Toast.makeText(App.getInstance(), msg, Toast.LENGTH_LONG).show();

                Crashlytics.log(msg);

            }
        });

    }


}