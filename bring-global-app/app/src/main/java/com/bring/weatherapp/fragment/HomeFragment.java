package com.bring.weatherapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bring.weatherapp.App;
import com.bring.weatherapp.R;
import com.bring.weatherapp.activity.AddNewCityActivity;
import com.bring.weatherapp.adapter.CityAdapter;
import com.bring.weatherapp.api.FindWeatherAPI;
import com.bring.weatherapp.model.APIError;
import com.bring.weatherapp.model.FindCitiesObject;
import com.bring.weatherapp.model.WeatherObject;
import com.bring.weatherapp.util.SaveLocal;
import com.bring.weatherapp.util.Utils;
import com.crashlytics.android.Crashlytics;

import java.util.List;

/**
 * Fragment
 *
 * @author Caio Bulgarelli
 */
public class HomeFragment extends Fragment {

    private TextView mEdit;
    private LinearLayout mAddNew;


    private RecyclerView mRecyclerView;
    private CityAdapter adapter;
    private LinearLayout mEmptyListView;


    public static final HomeFragment newInstance() {

        HomeFragment thisFragment = new HomeFragment();

        Bundle args = new Bundle();
        thisFragment.setArguments(args);

        return thisFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, null);

        // Bind views
        mEdit = v.findViewById(R.id.tv_edit);
        mAddNew = v.findViewById(R.id.ll_add_new_city);
        mRecyclerView = v.findViewById(R.id.recyclerView);
        mEmptyListView = v.findViewById(R.id.ll_empty_list);


        mEmptyListView.setVisibility(View.GONE);

        // Listener
        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!adapter.isEditable()) {
                    mEdit.setText("Done");

                    adapter.setEditable(true);
                    adapter.notifyDataSetChanged();
                } else {
                    mEdit.setText("Edit");

                    adapter.setEditable(false);
                    adapter.notifyDataSetChanged();

                }

            }
        });
        mAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), AddNewCityActivity.class);
                startActivity(i);

            }
        });

        return v;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onResume() {
        super.onResume();

        updateUI();
    }


    /*
     * Update UI
     */
    private void updateUI() {


        List<WeatherObject> list = SaveLocal.getListCitiesSearched();
        if (list != null && list.size() > 0) {

            mRecyclerView.setVisibility(View.VISIBLE);

            // set up the RecyclerView
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
            adapter = new CityAdapter(getActivity(), getChildFragmentManager(), list);
            mRecyclerView.setAdapter(adapter);

        } else {
            displayEmptyList();
        }


    }

    private void displayEmptyList() {

        mRecyclerView.setVisibility(View.GONE);
        mEmptyListView.setVisibility(View.VISIBLE);

    }


}