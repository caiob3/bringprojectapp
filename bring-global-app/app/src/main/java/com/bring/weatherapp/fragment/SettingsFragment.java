package com.bring.weatherapp.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.bring.weatherapp.R;
import com.bring.weatherapp.activity.MainActivity;
import com.crashlytics.android.Crashlytics;

/**
 * Fragment
 *
 * @author Caio Bulgarelli
 */
public class SettingsFragment extends Fragment {


    private LinearLayout llUserPref;
    private LinearLayout help;
    private LinearLayout shareThisApp;


    public static final SettingsFragment newInstance() {

        SettingsFragment thisFragment = new SettingsFragment();

        Bundle args = new Bundle();
        thisFragment.setArguments(args);

        return thisFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings, null);


        // Bind Views
        llUserPref = v.findViewById(R.id.ll_setup);
        help = v.findViewById(R.id.ll_help);
        shareThisApp = v.findViewById(R.id.ll_share_this_app);


        // Listener
        llUserPref.setOnClickListener(onClickSetupUserPref);
        help.setOnClickListener(onClickHelp);
        shareThisApp.setOnClickListener(onClickTellAFriend);


        return v;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        updateUI();
    }

    // User Preferences
    private View.OnClickListener onClickSetupUserPref = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            ((MainActivity) getActivity()).displayUserPrefFragment();

        }
    };


    // Help
    // This it would point to an URL such as
    // http://www.website.com/page/help
    // in order to display the help section.
    // For the purpose of this App Assigment,
    // the URL is pointing to Bring Global website
    private View.OnClickListener onClickHelp = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            try {

                String url = "http://www.bringglobal.com/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            } catch (Exception e) {
                Crashlytics.logException(e);
            }


        }
    };

    // Tell A Friend (Share this App)
    private View.OnClickListener onClickTellAFriend = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            String shareText = getString(R.string.share_text);

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        }
    };


    /*
     * Update UI
     */
    private void updateUI() {


    }



}