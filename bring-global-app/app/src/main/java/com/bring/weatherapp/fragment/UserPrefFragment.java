package com.bring.weatherapp.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bring.weatherapp.App;
import com.bring.weatherapp.R;
import com.bring.weatherapp.util.SaveLocal;

/**
 * Fragment
 *
 * @author Caio Bulgarelli
 */
public class UserPrefFragment extends Fragment {

    private ImageView ivArrowLeft;
    private Spinner spinnerLanguage;

    public static final UserPrefFragment newInstance() {

        UserPrefFragment thisFragment = new UserPrefFragment();

        Bundle args = new Bundle();
        thisFragment.setArguments(args);

        return thisFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_user_pref, null);


        // Bind Views
        ivArrowLeft = v.findViewById(R.id.iv_arrow_back);
        spinnerLanguage = v.findViewById(R.id.spinner_unit_system);


        // Listener
        spinnerLanguage.setOnItemSelectedListener(onItemLanguageSelected);
        ivArrowLeft.setOnClickListener(onClickArrowLeft);

        return v;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        updateUI();
    }

    private View.OnClickListener onClickArrowLeft = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            FragmentManager fm = getFragmentManager();
            if (fm.getBackStackEntryCount() > 0) {
                Log.i("UserPrefFragment", "popping backstack");
                fm.popBackStack();
            } else {
                Log.i("UserPrefFragment", "nothing on backstack, calling super");
            }

        }
    };



    /*
     * Update UI
     */
    private void updateUI() {


        updateSpinnerUnits();
    }



    private void updateSpinnerUnits() {


        String units = SaveLocal.getUnitSystemPreference();
        int position;

        switch (units) {

            case "imperial":
                position = 0;
                break;

            case "metric":
                position = 1;
                break;

            default:
                position = 0;
                break;

        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, App.arrayUnitsSystem);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLanguage.setAdapter(adapter);


        spinnerLanguage.setSelection(position);

    }

    private AdapterView.OnItemSelectedListener onItemLanguageSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            String s = "";

            switch (position) {

                case 0:
                    s = "imperial";
                    break;
                case 1:
                    s = "metric";
                    break;

            }

            SaveLocal.setUnitSystemPreference(s);

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // do nothing ;)
        }
    };



}