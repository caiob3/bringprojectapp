
package com.bring.weatherapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FindCitiesObject {

    @SerializedName("list")
    @Expose
    public List<WeatherObject> list;

    public List<WeatherObject> getList() {
        return list;
    }
}
