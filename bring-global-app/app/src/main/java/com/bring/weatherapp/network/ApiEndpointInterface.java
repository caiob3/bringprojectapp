package com.bring.weatherapp.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by caio
 */
public interface ApiEndpointInterface {


    //
    // Open Weather API
    // https://openweathermap.org/
    //
    public final String version = "2.5";



    @GET("/data/" + version + "/weather")
    Call<JsonObject> getByCityName(@Query("q") String cityName, @Query("units") String units);

    @GET("/data/" + version + "/find")
    Call<JsonObject> getByLonLat(@Query("lat") String lat, @Query("lon") String lon, @Query("cnt") int count, @Query("units") String units);



}