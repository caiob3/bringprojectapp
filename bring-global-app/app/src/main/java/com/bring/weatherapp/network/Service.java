package com.bring.weatherapp.network;


import android.util.Log;

import com.bring.weatherapp.App;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by caio
 */
public class Service {


    public static ApiEndpointInterface getService() {

        ApiEndpointInterface apiService =
                provideRetrofit().create(ApiEndpointInterface.class);

        return apiService;
    }

    public static Retrofit provideRetrofit() {

        String baseUrl = App.getAPIurl();
        Gson gson = new GsonBuilder().serializeNulls().create();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(provideOkHttpClient())
                .build();
    }


    private static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(provideCacheInterceptor())
                .cache(provideCache())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    private static Cache provideCache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(App.getInstance().getCacheDir(), "http-cache"),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {
            Log.e(App.TAG, "Could not create Cache!");
            Crashlytics.logException(e);
        }
        return cache;
    }


    public static Interceptor provideCacheInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {


                Request original = chain.request();


                // KEY Header
                String key = App.getAPIkey();


                String path = original.url().toString();
                String removeCharacter = path.replace("%2F", "/"); // replace

                // Customize the request
                Request request = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("x-api-key", key)
                        .method(original.method(), original.body())
                        .url(removeCharacter)
                        .build();

                Response response = chain.proceed(request);

                // Customize or return the response
                //return response;

                return response.newBuilder()
                        .removeHeader("Pragma")
                        .removeHeader("Cache-Control")
                        .header("Cache-Control", "public, max-age=" + 5)
                        .build();

            }
        };
    }



}