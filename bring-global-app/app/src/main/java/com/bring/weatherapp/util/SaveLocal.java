package com.bring.weatherapp.util;


import android.content.SharedPreferences;

import com.bring.weatherapp.App;
import com.bring.weatherapp.model.WeatherObject;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/*
 * Save into SharedPreference (as persistent data)
 */
public class SaveLocal {


    private static final String UNIT_SYSTEM_PREFERENCE = "unitSystemPref";
    private static final String LIST_CITIES_SEARCHED = "ListCitiesSearched";


    public static void setUnitSystemPreference(String u) {

        SharedPreferences sPref = App.getSharedPreferences();
        SharedPreferences.Editor editor = sPref.edit();

        editor.putString(UNIT_SYSTEM_PREFERENCE, u);
        editor.commit();
    }



    /*
        Temperature is available in Fahrenheit, Celsius and Kelvin units.

        For temperature in Fahrenheit use units=imperial
        For temperature in Celsius use units=metric
        Temperature in Kelvin is used by default, no need to use units parameter in API call
        List of all API parameters with units openweathermap.org/weather-data
     */
    public static String getUnitSystemPreference() {

        String u = App.getSharedPreferences().getString(UNIT_SYSTEM_PREFERENCE, "");

        if (u.isEmpty()) {

            // Default
            return "imperial";

        } else {
            return u;
        }

    }


    /**
     * Save city search
     */
    public static void saveInCitiesSearched(WeatherObject item) {

        SharedPreferences sPref = App.getSharedPreferences();
        SharedPreferences.Editor editor = sPref.edit();


        List<WeatherObject> fullList = getListCitiesSearched();
        fullList.add(item); // add Item (city)



        Gson gson = new Gson();
        String jsonString = gson.toJson(fullList);
        editor.putString(LIST_CITIES_SEARCHED, jsonString);
        editor.commit();


    }

    /**
     * Remove city searched
     */
    public static void removeCitySearched(WeatherObject item) {

        SharedPreferences sPref = App.getSharedPreferences();
        SharedPreferences.Editor editor = sPref.edit();

        List<WeatherObject> fullList = getListCitiesSearched();
        //List<WeatherObject> newList = new ArrayList<>();

        // to Remove
        for (int i=0 ; i < fullList.size() ; i++)
        {
            WeatherObject w = fullList.get(i);
            String sId = String.valueOf(w.getId());
            String itemId = String.valueOf(item.getId());
            if (sId.equalsIgnoreCase(itemId)) {
                fullList.remove(w);
            }
        }


        Gson gson = new Gson();
        String jsonString = gson.toJson(fullList);
        editor.putString(LIST_CITIES_SEARCHED, jsonString);
        editor.commit();


    }

    /**
     * Retrieve list cities
     */
    public static List<WeatherObject> getListCitiesSearched() {

        SharedPreferences sPref = App.getSharedPreferences();


        String json = sPref.getString(LIST_CITIES_SEARCHED, "");


        List<WeatherObject> list = new ArrayList<>();

        try {
            Type listType = new TypeToken<List<WeatherObject>>() {
            }.getType();
            list = new Gson().fromJson(json, listType);
        } catch (Exception e) {

            Crashlytics.logException(e);
        }

        if (list == null) {
            list = new ArrayList<>();
        }

        return list;


    }

}