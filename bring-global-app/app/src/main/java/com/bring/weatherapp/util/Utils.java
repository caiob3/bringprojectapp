package com.bring.weatherapp.util;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

import com.bring.weatherapp.App;

public class Utils {


    public static String toString(Object obj) {
        return obj != null ? obj.toString() : "";
    }


    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }


    /**
     * Get URL Icon weather
     */
    public static String getIconURL(String iconName) {

        String url = "http://openweathermap.org/img/wn/" + iconName + "@2x.png";

        return url;
    }

}
